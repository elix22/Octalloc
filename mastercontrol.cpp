/* Realm of Octalloc
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "effectmaster.h"
#include "inputmaster.h"
#include "player.h"
#include "spawnmaster.h"
#include "networkmaster.h"

#include "realm.h"
#include "head.h"
#include "lantern.h"
#include "barrel.h"

#include "mastercontrol.h"



 URHO3D_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl::MasterControl(Context *context):
    Application(context)
{
}

Scene*MasterControl::GetServerScene() const { return GetSubsystem<NetworkMaster>()->GetServerScene(); }

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs")+"Octalloc.log";
    engineParameters_[EP_WINDOW_TITLE] = "Octalloc";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";
    engineParameters_[EP_FULL_SCREEN] = false;
    engineParameters_[EP_WINDOW_WIDTH] = 640;
    engineParameters_[EP_WINDOW_HEIGHT] = 480;
}
void MasterControl::Start()
{
    RegisterObject<Realm>();
    RegisterObject<Head>();
    RegisterObject<Lantern>();
    RegisterObject<Barrel>();

    context_->RegisterSubsystem(this);
    RegisterSubsystem<EffectMaster>();
    RegisterSubsystem<InputMaster>();
    RegisterSubsystem<SpawnMaster>();
    RegisterSubsystem<NetworkMaster>();

    Graphics* graphics{ GetSubsystem<Graphics>() };
    if (graphics)
        GetSubsystem<Engine>()->SetMaxFps(graphics->GetRefreshRate());

    CreateScene();

    SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(MasterControl, HandlePostRenderUpdate));
}
void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = NewScene();

    CreateCameraAndViewport();

    PlayMusic();
}
Scene* MasterControl::NewScene()
{
    Scene* newScene{ new Scene(context_) };

    newScene->CreateComponent<Octree>(LOCAL);
    newScene->CreateComponent<PhysicsWorld>(LOCAL)->SetGravity(Vector3::ZERO);
    newScene->CreateComponent<DebugRenderer>(LOCAL);

    return newScene;
}
void MasterControl::CreateCameraAndViewport()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    Node* cameraNode{ scene_->CreateChild("Camera", LOCAL) };
    cameraNode->SetPosition(Vector3(-7.0f, 1.0, -7.0f));
    cameraNode->LookAt(Vector3::DOWN);
    Camera* camera{ cameraNode->CreateComponent<Camera>() };
    camera->SetNearClip(1.23f);
    camera->SetFov(60.0f);
    Viewport* viewport{ new Viewport(context_, scene_, camera) };

    SharedPtr<RenderPath> renderPath{ viewport->GetRenderPath()->Clone() };
    renderPath->Append(cache->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    renderPath->SetEnabled("FXAA3", true);
    renderPath->Append(cache->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    renderPath->SetShaderParameter("BloomHDRThreshold", 0.75f);
    renderPath->SetShaderParameter("BloomHDRMix", Vector2(1.0f, 1.0f));
    renderPath->SetEnabled("BloomHDR", true);
    renderPath->Append(cache->GetResource<XMLFile>("PostProcess/GreyScale.xml"));
    renderPath->SetEnabled("GreyScale", false);
    viewport->SetRenderPath(renderPath);

    GetSubsystem<Renderer>()->SetViewport(0, viewport);
}
void MasterControl::PlayMusic()
{
    Sound* music{ Fetch<Sound>("Kevin_MacLeod_-_Master_of_the_Feast.ogg") };
    music->SetLooped(true);
    Scene* musicScene{ new Scene(context_) };
    Node* musicNode{ musicScene->CreateChild("Music") };
    SoundSource* musicSource{ musicNode->CreateComponent<SoundSource>() };
    musicSource->SetSoundType(SOUND_MUSIC);
    musicSource->Play(music);

    musicSource->SetGain(0.23f);
}

Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}
Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p : players_) {

        if (p->GetPlayerId() == playerId){
            return p;
        }
    }
    return nullptr;
}
Player* MasterControl::GetNearestPlayer(Vector3 pos)
{
    Player* nearest{};
    for (Player* p : players_){
        if (p->IsAlive()){

            if (!nearest
                    || IM->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition().DistanceToPoint(pos) <
                       IM->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition().DistanceToPoint(pos))
            {
                nearest = p;
            }
        }
    }
    return nearest;
}

void MasterControl::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    return;

    GetScene()->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
}
