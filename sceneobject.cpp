/* Realm of Octalloc
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "networkmaster.h"
#include "spawnmaster.h"

#include "sceneobject.h"



SceneObject::SceneObject(Context *context):
    LogicComponent(context),
    initialSoundSources_{0},
    graphicsNode_{},
    randomizer_{Random()}
{
}

void SceneObject::OnNodeSet(Node *node)
{ if (!node) return;

    graphicsNode_ = node_->CreateChild("Graphics");

    while (sampleSources_.Size() < initialSoundSources_) {

        AddSoundSource();
    }
}

void SceneObject::Set(Vector3 position)
{
    node_->SetPosition(position);
    node_->SetEnabledRecursive(true);
}

void SceneObject::Disable()
{
    node_->SetEnabledRecursive(false);
}

void SceneObject::AddSoundSource()
{
    SoundSource3D* sampleSource{ node_->CreateComponent<SoundSource3D>(LOCAL) };
    sampleSource->SetSoundType(SOUND_EFFECT);

    sampleSources_.Push(sampleSource);
}
void SceneObject::PlaySample(Sound* sample, float gain)
{
    if (IsServerObject())
        return;

    for (SoundSource3D* s : sampleSources_) {

        if (!s->IsPlaying()) {

            s->SetGain(gain);
            s->Play(sample);
            return;
        }
    }

    AddSoundSource();
    PlaySample(sample, gain);
}

Vector3 SceneObject::GetWorldPosition() const
{
    return node_->GetWorldPosition();
}

bool SceneObject::IsServerObject() const
{
    return node_->GetScene() == GetSubsystem<NetworkMaster>()->GetServerScene();
}



