TARGET = octalloc

LIBS += \
    ../Octalloc/Urho3D/lib/libUrho3D.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    ../Octalloc/Urho3D/include \
    ../Octalloc/Urho3D/include/Urho3D/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
    luckey.h \
    mastercontrol.h \
    sceneobject.h \
    spawnmaster.h \
    inputmaster.h \
    player.h \
    controllable.h \
    effectmaster.h \
    head.h \
    lantern.h \
    networkmaster.h \
    realm.h \
    barrel.h

SOURCES += \
    luckey.cpp \
    mastercontrol.cpp \
    sceneobject.cpp \
    spawnmaster.cpp \
    inputmaster.cpp \
    player.cpp \
    controllable.cpp \
    effectmaster.cpp \
    head.cpp \
    lantern.cpp \
    networkmaster.cpp \
    realm.cpp \
    barrel.cpp

DISTFILES += \
    LICENSE_TEMPLATE
