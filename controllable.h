/* Realm of Octalloc
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef CONTROLLABLE_H
#define CONTROLLABLE_H

#include <Urho3D/Urho3D.h>
#include <bitset>

#include "sceneobject.h"

static const unsigned MOVE_RIGHT{   1 << 0 };
static const unsigned MOVE_LEFT{    1 << 1 };
static const unsigned MOVE_UP{      1 << 2 };
static const unsigned MOVE_DOWN{    1 << 3 };
static const unsigned MOVE_FORWARD{ 1 << 4 };
static const unsigned MOVE_BACK{    1 << 5 };

static const unsigned TURN_RIGHT{   1 << 6 };
static const unsigned TURN_LEFT{    1 << 7 };
static const unsigned TURN_UP{      1 << 8 };
static const unsigned TURN_DOWN{    1 << 9 };
static const unsigned TURN_CW{      1 << 10 };
static const unsigned TURN_CCW{     1 << 11 };

static const unsigned RUN{          1 << 12 };

class Controllable : public SceneObject
{
    friend class InputMaster;
    URHO3D_OBJECT(Controllable, SceneObject);
public:
    Controllable(Context* context);
    virtual void OnNodeSet(Node* node);

    void SetControls(const Controls& controls) noexcept { controls_ = controls; }
    const Controls& GetControls() const noexcept { return controls_; }

    Player* GetPlayer();
protected:
private:
    Controls controls_;
};

#endif // CONTROLLABLE_H
