/* Realm of Octalloc
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "lantern.h"



Lantern::Lantern(Context* context) : SceneObject(context)
{
}

void Lantern::OnNodeSet(Node* node)
{
    if (!node)
        return;

    SceneObject::OnNodeSet(node);

    AnimatedModel* model{ graphicsNode_->GetOrCreateComponent<AnimatedModel>() };
    model->SetModel(MC->Fetch<Model>("Lantern.mdl"));
    model->SetCastShadows(true);
    model->ApplyMaterialList();

    AnimationController* animControl{ graphicsNode_->GetOrCreateComponent<AnimationController>() };
    animControl->PlayExclusive("Models/Sway.ani", 0, true);
    animControl->SetSpeed("Models/Sway.ani", 0.23f + Random(0.05f));
    animControl->SetTime("Models/Sway.ani", Random(animControl->GetLength("Models/Sway.ani")));

    if (Node* endLink = graphicsNode_->GetChild("EndLink", true)) {

        Node* lightNode{ endLink->CreateChild("Light") };
        lightNode->Translate(Vector3::UP, TS_PARENT);
        Light* light{ lightNode->CreateComponent<Light>() };
        light->SetCastShadows(true);
        light->SetRange(27.0f);
        light->SetShadowNearFarRatio(0.12f);
        light->SetShadowBias(BiasParameters(0.0042f, 0.0042f));
        light->SetColor(Color(1.0f, 0.9f, 0.7f));
        light->SetBrightness(0.666f);
        light->SetShadowFadeDistance(10.0f);
        light->SetShadowDistance(16.0f);
        light->SetShadowFocus(FocusParameters(true, false, true, 23.0f, 42.0f));

        RigidBody* body{ lightNode->CreateComponent<RigidBody>() };
        body->SetKinematic(true);
        CollisionShape* collider{ endLink->CreateComponent<CollisionShape>() };
        collider->SetSphere(1.0f);
    }
}

void Lantern::Update(float timeStep)
{
}



